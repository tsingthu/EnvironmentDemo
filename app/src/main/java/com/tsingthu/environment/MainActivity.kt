package com.tsingthu.environment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.tsingthu.environment.config.TEXT_VALUE
import com.tsingthu.environment.extensions.toast
import exocr.idcard.IDCardManager

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<TextView>(R.id.text).text = TEXT_VALUE

        val iD_IMAGEMODE_HIGH = IDCardManager.ID_IMAGEMODE_HIGH
        this.toast("引入了依赖 其中有个常量的值为：$iD_IMAGEMODE_HIGH")
    }
}
