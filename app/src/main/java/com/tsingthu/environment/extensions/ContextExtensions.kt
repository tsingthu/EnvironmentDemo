package com.tsingthu.environment.extensions

import android.content.Context
import android.widget.Toast

/**
 * @author QinHan
 * @version V1.0
 * Description: 类说明
 * Created by Qin on 2019-07-01 18:31. Updated
 * Email: qinh@fenqi.im
 */
fun Context.toast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}